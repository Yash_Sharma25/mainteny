# README #
Generates a monthly schedules for technicians to service visits

### Approach ###
* Generate daily schedule by considering every technician visits 5 places a day
* Service time considered to be 60 mins and is added in the time matrix

### Test sample ###
* Schedule generated for 4 technicians
* ![Test](images/test.png)

### Limitations ###
* The emergency scenario would require us knowing the current locations of all technicians
* Approach to solve is described as algorithm but not code

### How do I get set up? ###
* pip install ortools
* pip install pandas
* python optimize.py
