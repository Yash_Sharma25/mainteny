"""Vehicles Routing Problem (VRP) with Time Windows."""
from __future__ import division
from __future__ import print_function
from ortools.constraint_solver import routing_enums_pb2
from ortools.constraint_solver import pywrapcp
from urllib.request import urlopen


import pandas as pds

import requests
import json

def create_data():
  """Creates the data."""
  data = {}

  file =('mainteny.xlsx')
  newData = pds.read_excel(file)
  newData['Last visit'] = pds.to_datetime(newData['Last visit'])

  #sort by last visit so that the most overdue customer is considered first
  newData = newData.sort_values(by=['Last visit'])
  data['API_key'] = 'AIzaSyCbl9uMGsldUysJwAkWnoF3MataSKeuZ10'

  data['addresses'] = newData['Address'] + ' ' + newData["ZIP Code"].astype(str)
  data['customers'] = newData['Customer'] + ' ' + newData['Customer ID'].astype(str).replace('\.0', '', regex=True)

  # add depot location as Berlin
  depotInfo = pds.Series(['Berlin'])
  data['addresses'] = pds.concat([depotInfo, data['addresses']])

  #print(data['addresses'])
  return data
  
def create_distance_matrix(data, startIndex, endIndex):
  addresses = []
  addresses = data["addresses"]
  addresses = addresses[startIndex:endIndex]
  print(addresses)
  API_key = data["API_key"]
  # Distance Matrix API only accepts 100 elements per request, so get rows in multiple requests.
  max_elements = 100
  num_addresses = len(addresses) # 16 in this example.
  # Maximum number of rows that can be computed per request (6 in this example).
  max_rows = max_elements // num_addresses
  # num_addresses = q * max_rows + r (q = 2 and r = 4 in this example).
  q, r = divmod(num_addresses, max_rows)
  dest_addresses = addresses
  distance_matrix = []
  time_matrix = []
  # Send q requests, returning max_rows rows per request.
  for i in range(q):
    origin_addresses = addresses[i * max_rows: (i + 1) * max_rows]
    response = send_request(origin_addresses, dest_addresses, API_key)
    distance_matrix += build_distance_matrix(response)
    time_matrix += build_time_matrix(response)

  # Get the remaining remaining r rows, if necessary.
  if r > 0:
    origin_addresses = addresses[q * max_rows: q * max_rows + r]
    response = send_request(origin_addresses, dest_addresses, API_key)
    distance_matrix += build_distance_matrix(response)
    time_matrix += build_time_matrix(response)

  data['time_matrix'] = time_matrix
  return distance_matrix

def send_request(origin_addresses, dest_addresses, API_key):
  """ Build and send request for the given origin and destination addresses."""
  def build_address_str(addresses):
    # Build a pipe-separated string of addresses
    address_str = ''
    for i in range(len(addresses) - 1):
      address_str += addresses.iloc[i] + '|'
    address_str += addresses.iloc[-1]
    return address_str

  request = 'https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial'
  origin_address_str = build_address_str(origin_addresses)
  origin_address_str = origin_address_str.replace("/","-")

  dest_address_str = build_address_str(dest_addresses)
  dest_address_str = dest_address_str.replace("/","-")

  request = request + '&origins=' + origin_address_str + '&destinations=' + \
                       dest_address_str + '&key=' + API_key

  request = replace_umlauts(request)
  #spaces are not allowed in URL
  request = request.replace(" ","+")

  jsonResult = urlopen(request)
  response = json.loads(jsonResult.read())
  return response

def build_distance_matrix(response):
  distance_matrix = []                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
  for row in response['rows']:
    row_list = [row['elements'][j]['distance']['value'] for j in range(len(row['elements']))]
    distance_matrix.append(row_list)                                                                                                                    
   
  return distance_matrix

def build_time_matrix(response):
  time_matrix = []                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   
  for row in response['rows']:
    row_time_list = [row['elements'][j]['duration']['value'] for j in range(len(row['elements']))]
    time_matrix.append(row_time_list)                                                                                                                       
   
  return time_matrix

replacements = {
    'ü': 'ue',
    'ä': 'ae',
    'ö': 'oe',
    'ß': 'ss',
}

def replace_umlauts(text: str) -> str:
    for find, replace in replacements.items():
        text = text.replace(find, replace)
    return text

#######################################################################

def create_data_model(data):
    """Stores the data for the problem."""
    data['depot'] = 0
    return data


def print_solution(data, manager, routing, solution):
    """Prints solution on console."""
    print(f'Objective: {solution.ObjectiveValue()}')
    time_dimension = routing.GetDimensionOrDie('Time')
    total_time = 0
    for vehicle_id in range(data['num_vehicles']):
        index = routing.Start(vehicle_id)
        plan_output = 'Route for vehicle {}:\n'.format(vehicle_id)
        while not routing.IsEnd(index):
            time_var = time_dimension.CumulVar(index)
            plan_output += '{0} Time({1},{2}) -> '.format(
                data['customers'][index], solution.Min(time_var),
                solution.Max(time_var))
            index = solution.Value(routing.NextVar(index))
        time_var = time_dimension.CumulVar(index)

        # time is in seconds, convert to minutes
        startTime = int(solution.Min(time_var)) / 60
        endTime = int(solution.Max(time_var)) / 60

        plan_output += '{0} Time({1},{2})\n'.format(data['customers'][index],
                                                    startTime,
                                                    endTime)
        plan_output += 'Time of the route: {}min\n'.format(startTime)
        print(plan_output)
        total_time += startTime
    print('Total time of all routes: {}min'.format(total_time))

def emergency_call(newLat, newLon):
  # get current locations of all technicians
  # find the closest technician using newLat, newLon to location of the technicians
  # using the current time, find the time_window in which the technician is working
  # assign next time_window as the emergency one
  # Rerun the solver for the remaining locations
  return

def main():
    """Solve the VRP with time windows."""
    # Instantiate the data problem.
    data = create_data()
    startIndex = 0

    # run schedules daily
    while startIndex < len(data['addresses']):
      data['num_vehicles'] = int(input('Enter no. of technicians:'))
      data = create_data_model(data)

      # consider an average of 5 visits by each technician per day
      # read no. of technicians * 5 rows for a day's schedule
      # iterate until end
      number_of_visits_for_day = data['num_vehicles'] * 5
      distance_matrix = create_distance_matrix(data, startIndex, startIndex + number_of_visits_for_day)

      startIndex += number_of_visits_for_day
      #print(distance_matrix)

      # add service time of 60 mins = 3600 sec except from depot to locations
      for i in range(len(data['time_matrix'])):
          for j in range(len(data['time_matrix'])):
              if i!=0 and i!=j:
                data['time_matrix'][i][j] =  data['time_matrix'][i][j] + 3600

      #print(data['time_matrix'])

      # Create the routing index manager.
      manager = pywrapcp.RoutingIndexManager(len(data['time_matrix']),
                                            data['num_vehicles'], data['depot'])

      # Create Routing Model.
      routing = pywrapcp.RoutingModel(manager)

      # Create and register a transit callback.
      def time_callback(from_index, to_index):
          """Returns the travel time between the two nodes."""
          # Convert from routing variable Index to time matrix NodeIndex.
          from_node = manager.IndexToNode(from_index)
          to_node = manager.IndexToNode(to_index)
          return data['time_matrix'][from_node][to_node]

      transit_callback_index = routing.RegisterTransitCallback(time_callback)

      # Define cost of each arc.
      routing.SetArcCostEvaluatorOfAllVehicles(transit_callback_index)

      # Add Time Windows constraint.
      time = 'Time'
      routing.AddDimension(
          transit_callback_index,
          30,  # allow waiting time
          32400,  # maximum time per vehicle
          False,  # Don't force start cumul to zero.
          time)
      time_dimension = routing.GetDimensionOrDie(time)
      # Add time window constraints for each location except depot.
      #7-4pm = 9 hrs = 32400 sec.
      for location_idx in range(1, len(data['time_matrix'])):
          if location_idx == data['depot']:
              continue
          index = manager.NodeToIndex(location_idx)
          time_dimension.CumulVar(index).SetRange(0, 32400)
      # Add time window constraints for each vehicle start node.
      depot_idx = data['depot']
      for vehicle_id in range(data['num_vehicles']):
          index = routing.Start(vehicle_id)
          time_dimension.CumulVar(index).SetRange(
              0,
              32400)

      # Instantiate route start and end times to produce feasible times.
      for i in range(data['num_vehicles']):
          routing.AddVariableMinimizedByFinalizer(
              time_dimension.CumulVar(routing.Start(i)))
          routing.AddVariableMinimizedByFinalizer(
              time_dimension.CumulVar(routing.End(i)))

      # Setting first solution heuristic.
      search_parameters = pywrapcp.DefaultRoutingSearchParameters()
      search_parameters.first_solution_strategy = (
          routing_enums_pb2.FirstSolutionStrategy.PATH_CHEAPEST_ARC)

      # Solve the problem.
      solution = routing.SolveWithParameters(search_parameters)

      # Print solution on console.
      if solution:
          print_solution(data, manager, routing, solution)
      else:
          print("No solution")

if __name__ == '__main__':
    main()